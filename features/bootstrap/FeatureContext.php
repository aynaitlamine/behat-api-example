<?php

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Imbo\BehatApiExtension\Context\ApiContext;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends ApiContext implements Context
{
    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }
    
    
    /**
     * @Given the “Accept” request header is Application\/Json
     */
    public function theAcceptRequestHeaderIsApplicationJson()
    {

        $this->setRequestHeader("Content-Type", "Application/Json");
        $this->requestPath('/users');
        $this->assertResponseHeaderExists("Content-Type", "Application/Json");
    }

     /**
     * @When I request “\/users\/:arg1” using HTTP GET
     */
    public function iRequestUsersUsingHttpGet($userID)
    {
        $this->requestPath("/users/" . $userID);
    }

    /**
     * @Then I should see :arg1 in users list
     */
    public function iShouldSeeInUsersList($userEmail)
    {
        $this->assertResponseBodyContainsJson($userEmail);
    }   

    /**
     * @Then try to update user with ID :userID:
     */
    public function tryToUpdateUserWithId($userID, PyStringNode $body)
    {
        $this->setRequestBody($body);

        $this->requestPath("/users/". $userID, "PATCH");
    }

    /**
     * @Then try to delete user with ID :userID
     */
    public function tryToDeleteUserWithId($userID)
    {
        $this->requestPath("/users/". $userID, "DELETE");
    }
}
