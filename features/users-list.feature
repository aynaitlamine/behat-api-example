@api
Feature: Test api functional
    Scenario: Get users list
    Given the “Accept” request header is Application/Json
    When I request “/users/1” using HTTP GET
    Then the response body contains JSON:  
    """
    {
        "email": "Sincere@april.biz"
    }
    """
    And try to update user with ID "1": 
    """
    {
        "email": "ayoub@example.com"
    }
    """
    Then try to delete user with ID "1"
